#!/bin/sh

# https://docs.gitlab.com/ee/ci/ssh_keys/README.html#ssh-keys-when-using-the-docker-executor

# Install ssh-agent if not already installed, it is required by Docker.
# Debian-based Linux:
if ( which apt-get ); # then echo 'debian-based';
  then which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y );
fi
# Alpine Linux:
if ( which apk ); # then echo 'alpine';
  then which ssh-agent || ( apk update && apk add openssh-client );
fi

# Run ssh-agent (inside the build environment)
eval $(ssh-agent -s)

# Add the SSH key stored in SSH_PRIVATE_KEY variable to the agent store
# ssh-add <(echo "$SSH_PRIVATE_KEY")
# Linux Alpine:
echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -

mkdir -p ~/.ssh
chmod 700 ~/.ssh

echo "$SSH_KNOWN_HOSTS" >> ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts

# Manually access servers you want to deploy to, and
# append the public key to .ssh/authorized_keys
