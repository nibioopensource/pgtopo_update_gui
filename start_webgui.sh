#!/bin/sh

usage() {
  echo "Usage: $0 [--force-build] <postgrest_port>"
}

BUILD=no
POSTGREST_PORT=

while test -n "$1"; do
  if test "$1" = "--force-build"; then
    BUILD=yes
  elif test -z "${POSTGREST_PORT}"; then
    POSTGREST_PORT="$1"
  else
    echo "Unknown option '$1'" >&1
    usage >&2
    exit 1
  fi
  shift

done

test -n "$POSTGREST_PORT" || {
  usage >&2
  exit 1
}


BASEDIR=$(cd $(dirname $0) && pwd -P)

cd ${BASEDIR} || exit 1

# Install dependencies and build
if test "${BUILD}" = "yes"; then
  npm ci && npm run build || exit 1
fi

cleanup() {
  echo "Cleaning up"
  rm -f ${BASEDIR}/src/.env.local
}

sig_cleanup() {
  trap '' EXIT
  cleanup
}

trap 'cleanup' EXIT
trap 'sig_cleanup' INT TERM

test -f src/.env.local && {
  ls -l src/.env.local
  cat src/.env.local
  echo "src/.env.local already exists, is there another gui running?" >&2
  exit 1
}

cat <<EOF | tee src/.env.local || exit 1
VITE_SERVER=http://localhost:${POSTGREST_PORT}
VITE_API=
EOF

npm start
