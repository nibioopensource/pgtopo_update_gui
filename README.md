# pgtopo_update_gui

This is a generic user interface for `pgtopo_update_sql`,
that uses `pgtopo_update_rest` to update spatial data stored in Postgis Topology.

## Requirements

To build the application (also to serve it?)
you need nodejs: '^14.18.0 || >=16.0.0'

Latest LTS as for 11/08/2022 is 16.16.0 and can be obtained from
[official website](https://nodejs.org/en/) or by using
[nvm](https://github.com/nvm-sh/nvm#installing-and-updating).

Example of installing and using latest nodejs LTS with nvm:

    # Install nvm if you don't have it yet
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
    source ~/.bashrc

    # Install latest LTS if you don't have it yet
    nvm install --lts

    nvm use --lts

## Configuring API endpoint

Through environment variables, defined by creating a
[dotenv](https://vitejs.dev/guide/env-and-mode.html#env-files) file under ./src
```shell
VITE_SERVER=https://[server_name]
VITE_API=[endpoint_name]
```

## Install dependencies

```shell
npm ci
```

## Building the app

```shell
npm run build
```

## Serving the app

```shell
npm run serve
```

## License
License can be found [here](LICENSE)

