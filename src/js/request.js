import Feature from 'ol/Feature';
import GeoJSON from 'ol/format/GeoJSON';

import { readDatasetConfiguration } from './decide';
import { clearDrawings } from './map/draw';
import { removeFeatureById } from './map/feature';
import { getBoundingBoxDiagonal, getEPSG } from './map/tools';
import { loadGeometry, removeGeometry } from './map/tools';
import { warn } from './notify';
import { rememberAsOriginalAttributes } from './storage';
import { createLayersList, moveTo } from './wizard';

const debug = false;
const env = import.meta.env;
const api = `${env.VITE_SERVER}${env.VITE_API ? '/' : ''}${env.VITE_API}`;
console.log('Using back-end:', api);
let app, dbSRID;

// curl ${api}/rpc/list_available_applications
export const listEditableLayers = () => {
  // console.log('listEditableLayers');
  const url = `${api}/rpc/list_available_applications`;
  fetch(url).then(res => res.json()).then(res => {
    // console.log(res);
    const list = res.data ?? ['ar5'];
    createLayersList(list);
    // When only one layer, no need for the user to select it:
    if (list.length === 1) configureBasedOnLayer(list[0]);
  }).catch(err => { // code to handle request errors
    console.log('Back-end is not ready!', err);
    createLayersList(['ar5']);
  });
};

// curl -X POST -H "Content-Type: application/json" -d '{"app_name": "ar5"}' \
//  ${api}/rpc/get_application_config
export const configureBasedOnLayer = (layerName) => {
  console.log('Ask for configuration (get_application_config)');
  app = layerName; // save it for future requests
  datasetName.innerHTML = `"${app}"`;

  const url = `${api}/rpc/get_application_config`;

  const request = new Request(url, {
    method: 'POST',
    body: JSON.stringify({ request: { app_name: app } }),
    headers: new Headers({ 'Content-Type': 'application/json' })
  });

  fetch(request).then(res => res.json()).then(res => {
    dbSRID = res?.data?.topology?.srid ?? 4326;
    readDatasetConfiguration(res.data);
    requestGeometry(true); // first call is manual, then triggered by map move
    moveTo('home');
  }).catch(err => { // code to handle request errors
    console.log('Back-end is not ready!', err);
    warn('Back-end is not ready!');
    moveTo('home');
  });
};

export const requestGeometry = (firstTime) => {
  console.log('requestGeometry (get_features_topojson)');
  const url = `${api}/rpc/get_features_topojson`;

  const json = { app_name: app, bbox_diagonal: getBoundingBoxDiagonal() };

  const request = new Request(url, {
    method: 'POST',
    body: JSON.stringify({ request: json }),
    headers: new Headers({ 'Content-Type': 'application/json' })
  });

  removeGeometry();
  // there should be a previous fetch abortion here
  fetch(request).then(res => res.json()).then(res => {
    const topojson = res.data;
    const points = topojson.objects.collection.geometries?.length > 0;
    const foundGeometry = (topojson?.arcs?.length > 0 || points);
    if (foundGeometry) loadGeometry(topojson, firstTime);
    else console.log('Empty TopoJSON');
  }).catch(err => { // code to handle request errors
    console.log('Back-end is not ready!', err);
    warn('Back-end is not ready!');
  });
};

export const saveFeatures = (ff) => {
  rememberAsOriginalAttributes(ff);
  const f = ff.item(0);
  const existingFeature = f.get('id');
  if (!existingFeature) sendFeature(f); else sendAttributes(f);
};

const sendFeature = (f) => { // single feature, can be MultiLineString
  const type = document.querySelector('input[name="geoRadio"]:checked').value;
  const point = (type === 'point');
  const surface = (type === 'surface');
  const func = `add_${surface ? 'borders_split_surfaces' : type}`;
  console.log(`sendFeature (${func})`);
  const url = `${api}/rpc/${func}`;
  const geojson = new GeoJSON().writeFeatureObject(f);
  geojson.geometry.crs = { properties: { name: getEPSG() }, type: 'name' };
  if (!geojson.properties) geojson.properties = {};
  const n = { new: geojson.properties };
  if (!point)  geojson.properties = surface ? { Border: n, Surface: n } : n;
  const json = { app_name: app };
  json[surface ? 'borderset' : type] = geojson;

  const request = new Request(url, {
    method: 'POST',
    body: JSON.stringify({ request : json }),
    headers: new Headers({ 'Content-Type': 'application/json' })
  });

  fetch(request).then(res => res.json()).then(res => {
    console.log(res);
    if (res.status === 'success') requestGeometry(); // to see new changes
    else warn(res.data?.raised_exception?.message || 'Something failed!');
  }).catch(err => { // code to handle request errors
    warn('Back-end is not ready!');
    console.log(err);
  });
  // console.log('Get new geometries to select for attributes definition');
  // console.log('clear drawings from map');
  // clearDrawings();
  // console.log('move wizard back to attributesSelection');
  // moveTo('attributesSelection');
  // backToStart(); // called in save()
};

const sendAttributes = (f) => { // single feature, can be MultiLineString
  const func = 'update_attributes';
  console.log(`sendAttributes (${func})`);
  const url = `${api}/rpc/${func}`;
  const type = document.querySelector('input[name="geoRadio"]:checked').value;
  console.log(f.getProperties(), 'table_name could be saved here');
  const json = {
    app_name: app,
    table_name: type, // get it from get_application_config
    id: f.get('id'),
    // properties: { label: 'updated' }
    properties: f.getProperties() // exclude geometry and id
  };

  const request = new Request(url, {
    method: 'POST',
    body: JSON.stringify({ request : json }),
    headers: new Headers({ 'Content-Type': 'application/json' })
  });

  fetch(request).then(res => res.json()).then(r => {
    console.log(r);
    if (r.status !== 'success')
      warn(r.data?.raised_exception?.message || 'Something failed!');
  }).catch(err => { // code to handle request errors
    console.log('Back-end is not ready!', err);
  });
  // backToStart(); // called in save()
};

// document.getElementById('sendBtn').onclick = sendFeature;
// document.getElementById('saveBtn').onclick = saveAttributes;

export const removeById = (id) => { // i.e. merge adjacent surfaces
  const type = document.querySelector('input[name="geoRadio"]:checked').value;
  const surface = (type === 'surface');
  const func = `remove_${surface ? 'borders_merge_surfaces' : type}`;
  console.log(`remove (${func})`);
  const url = `${api}/rpc/${func}`;
  const json = { app_name: app, border_id: id };
  json[`${type}_id`] = id;

  const request = new Request(url, {
    method: 'POST',
    body: JSON.stringify({ request : json }),
    headers: new Headers({ 'Content-Type': 'application/json' })
  });

  fetch(request).then(res => res.json()).then(r => {
    const success = (r.status === 'success');
    console.log(r);
    if (success) removeFeatureById(id); // requestGeometry(); // to see new changes
    else warn(r.data?.raised_exception?.message || 'Something failed!');
  }).catch(err => { // code to handle request errors
    warn('Back-end is not ready!');
    console.log('Back-end is not ready!', err);
  });
};

export const movePoint = (f) => {
  // console.log(f.getGeometry().getType()); // api fails with MultiPoint
  const p = f.getGeometry().getPoint(0); // MultiPoint --> Point
  const func = 'move_point';
  console.log(`move (${func})`);
  const url = `${api}/rpc/${func}`;
  const geojson = new GeoJSON().writeFeatureObject(new Feature(p));
  geojson.geometry.type = 'Point';
  geojson.geometry.crs = { properties: { name: getEPSG() }, type: 'name' };
  const json = { app_name: app, point_id: f.get('id'), point: geojson };

  const request = new Request(url, {
    method: 'POST',
    body: JSON.stringify({ request : json }),
    headers: new Headers({ 'Content-Type': 'application/json' })
  });

  fetch(request).then(res => res.json()).then(r => {
    const success = (r.status === 'success');
    console.log(r);
    if (success) console.log('Point moved');
    else warn(r.data?.raised_exception?.message || 'Something failed!');
  }).catch(err => { // code to handle request errors
    warn('Back-end is not ready!');
    console.log('Back-end is not ready!', err);
  });
};
