import '../css/wizard.css';

import { dragElement } from './drag'
import { updateSelected } from './map/tools'
import { configureBasedOnLayer, requestGeometry } from './request'

const stage = ['layerSelection', 'home', 'attributesSelection'];

let current = 'layerSelection';

const move = (from, to) => {
  const f = document.getElementById(from);
  const t = document.getElementById(to);
  f.hidden = true;
  t.hidden = false;
  current = to;
};

const wizard = document.getElementById('wizard');
const results = document.getElementById('resultsContent');
// Keep window narrow on start
const splitter = document.getElementById('sentenceSplit');
const resizeObserver = new ResizeObserver(() => {
  if (wizard.style.width) {
    splitter.style.display = 'none';
    resizeObserver.unobserve(wizard);
  }
});

export const moveTo = (stage) => move(current, stage);

export const getStage = () => current;

export const initialise = () => {
  // Make side and top borders draggagle:
  dragElement('wizard', 'topDragHandle');
  dragElement('wizard', 'sideDragHandle');

  closeBtn.onclick = () => wizard.style.display = 'none';
  editBtn.onclick = () => {
    console.log('activate ol select interaction to pick a feature to update');
    console.log('load feature attributes');
    moveTo('attributesSelection');
  };
  backBtn.onclick = () => moveTo('layerSelection');
  resizeObserver.observe(wizard);
};

const setVisibility = (v) => {
  wizard.style.display = v;
  wizardSwitch.style.display = v;
};
export const hide = () => setVisibility('none');
export const show = () => setVisibility('block');

export const createLayersList = (array) => {
  const container = document.getElementById('layerSelection');
  const ul = document.createElement('ul');
  container.appendChild(ul);
  array.forEach(layer => {
    const li = document.createElement('li');
    const btn = document.createElement('button');
    btn.id = layer;
    btn.className = 'green-transition custom-btn layerBtn';
    btn.innerHTML = layer;
    btn.onclick = () => configureBasedOnLayer(btn.id);
    li.appendChild(btn);
    ul.appendChild(li);
  });
};

export const initializeAttributesSelection = (aList) => {
  console.log('Generate attributes selectors');
  attributes.innerHTML = '';
  aList.forEach(a => attributes.appendChild(makeFormElement(a)));
};

const makeFormElement = (a) => {
  // console.log(a.name, a.description, a.type, a.display);
  const li = document.createElement('li');
  let e = null;
  if (a.options && a.options.length > 0) {
    e = document.createElement('select');
    a.options.forEach(ao => {
      const o = document.createElement('option');
      o.value = ao.value;
      o.innerHTML = ao.description;
      e.appendChild(o);
    });
  } else {
    e = document.createElement('input');
    if (a.type === 'date') {
      e.type = 'date';
      const today = new Date();
      const isoToday = today.toISOString().substring(0, 10); // yyyy-mm-dd
      // e.valueAsDate = new Date(); // today // did not work for webkit
      e.defaultValue = isoToday;
      e.value = isoToday;
      e.min = '2000-01-01';
      e.max = isoToday;
    } else {
      console.log('Not sure what to do with this type of attribute');
      e.type = 'text';
    }
  }
  // e.id = ´attribute_${a.name}´; // invalid character (probably Vite)
  e.id = 'attribute_' + a.name;
  e.name = a.name; // e.id must be unique, a.name can lead to a non unique
  // Associate label:
  const l = document.createElement('label');
  l.htmlFor = e.id;
  l.innerHTML = a.description ?? a.name;
  li.appendChild(l);
  li.appendChild(document.createElement('br'));
  li.appendChild(e);
  // save changes to feature to update style, but store original value:
  e.oninput = (evt) => updateSelected(evt.target);
  return li;
};

export const loadFeatureAttributes = (f) => {
  [...attributes.children].forEach(li => {
    const i = li.querySelector('input');
    i.value = f.get(i.name) ?? '';
  });
  moveTo('attributesSelection');
};

export const showRemovingOption = (id, removing) => {
  // console.log('Show merging option', borderId);
  const b = document.getElementById('attributesSelection');
  b.classList.add(removing ? 'remove-border' : 'merge-surfaces');
  b.classList.remove(!removing ? 'remove-border' : 'merge-surfaces');
  // b.removeAttribute('hidden');
  // b.classList.remove('hidden');
  removeBtn.dataset.id = id;
};
