// Keep track of which attributes temporarily changed during editing.
// Use "change" variable to save and "originals" variable to regret.

const changed = {};

// original is not common to every selected features like changed:
const originals = {};

const forget = (obj) => Object.keys(obj).forEach(k => delete obj[k]);

export const changingTo = (key,value) => changed[key] = value;

export const forgetChanges = () => forget(changed);

export const rememberAsOriginalAttributes = (features) => {
  forget(originals);
  // getLayerById('topology').getSource().getFeatures().forEach(f => {
  features.forEach(f => originals[f.get('id')] = f.getProperties());
};

export const resetToOriginalAttributes = (features) => {
  // document.getElementById('regretButton').onclick = () => {
  // getLayerById('topology').getSource().getFeatures().forEach(f => {
  features.forEach(f => f.setProperties(originals[f.get('id')]));
};

export const updateFeaturesWithCurrentChanges = (features) => {
  features.forEach(f => f.setProperties(changed));
};

