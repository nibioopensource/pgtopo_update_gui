import '../css/notify.css';

const notificationBar = document.createElement('div');
const notification = document.createElement('span');

const notify = (msg, type) => {
  console.log(msg);
  notification.innerHTML = msg;
  notificationBar.className = '';
  notificationBar.classList.add(type);
  // Workaround to fix first notification not animating:
  //  setTimeout(() => notificationBar.classList.add(type), 9);
  setTimeout(() => notificationBar.className = '', 15000);
};

export const info = (msg) => notify(msg, 'info');
export const warn = (msg) => notify(msg, 'warn');

export const initialize = () => {
  notificationBar.id = 'notification';
  const x = document.createElement('span');
  x.className = 'x';
  x.innerHTML = '&times;';
  x.onclick = () => notificationBar.className = '';
  notificationBar.appendChild(notification);
  notificationBar.appendChild(x);
  document.body.appendChild(notificationBar);
};
