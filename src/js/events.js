import { clearDrawings, enableModify } from './map/draw'
import {
  getAdjacentFeatures, getMap, getSelect, updateGeometry
} from './map/tools'
import { movePoint, removeById, requestGeometry, saveFeatures } from './request'
import {
  forgetChanges, rememberAsOriginalAttributes,
  resetToOriginalAttributes, updateFeaturesWithCurrentChanges
} from './storage'
import {
  getStage, loadFeatureAttributes, moveTo, showRemovingOption
} from './wizard'

const map = getMap();

let movingPoint; // save Modify interaction when selecting a single point

export const listenToEvents = () => {
  map.on('pointermove', e => {
    const cursorOnFeature = map.hasFeatureAtPixel(e.pixel);
    map.getViewport().style.cursor = cursorOnFeature ? 'pointer': '';
    // if (cursorOnFeature) map.forEachFeatureAtPixel(e.pixel, f => {
    //   highlightCorrespondingListItem(f);
    // });
    // else removeListItemHighlight();
  });

  map.on('moveend', () => {
    // console.log('moveend');
    // updateProjAndExtent();
    // enableEditButtons(); // disables when zooming out and backend not queried
    // updateProjection();
    if (getStage() !== 'home') return; // no need to fetch data
    updateGeometry();
  });

  const select = getSelect();
  select.on('select', e => {
    // console.log(e.selected.length, e.deselected.length);
    if (movingPoint) movingPoint.setActive(false);
    let f = null;
    resetToOriginalAttributes(e.deselected);
    const stillSelected = select.getFeatures();
    const l = stillSelected.getLength();
    if (e.selected.length === 0) { // on deselection
      // use one of the remaining selected features, if any
      if (l === 0) {
        moveTo('home');
        forgetChanges();
      } else f = stillSelected.item(l - 1);
    } else {
      // length is 1 on selection, even by using shift for multiple
      // length is greater than 1 when using a selection box
      f = e.selected[0];
      // Update new selected features with current changes, if any.
      updateFeaturesWithCurrentChanges(e.selected);
    }
    if (f) loadFeatureAttributes(f);
    // Give possibility to merge when:
    //  - only one linestring is selected and it is shared by two faces
    //  - only two polygons are selected and they share a common border
    // removeBtn.addAttribute('hidden');
    // removeBtn.classList.add('hidden');
    attributesSelection.classList.remove('remove-border', 'merge-surfaces');
    const ff = stillSelected.getArray();
    const t = ff.map(f => f.getGeometry().getType());
    const type = document.querySelector('input[name="geoRadio"]:checked').value;
    if (l === 1 && t[0] === 'MultiLineString' && type === 'surface') {
      console.log('Single MultiLineString has been selected');
      const adjacent = getAdjacentFeatures(f, 'MultiPolygon');
      console.log('Adjacent polygons', adjacent.map(f => f.get('id')));
      if (adjacent.length === 2) showRemovingOption(f.get('id'), true);
    } else if (l === 2 && t[0] === 'MultiPolygon' && t[1] === 'MultiPolygon') {
      console.log('MultiPolygon couple has been selected');
      const adj = ff.map(f => getAdjacentFeatures(f, 'MultiLineString'));
      const common = adj[0].filter(f => adj[1].includes(f));
      console.log('Shared linestrings', common.map(f => f.get('id')));
      if (common.length > 0) showRemovingOption(common[0].get('id'), false);
    } else if (l === 1 && type !== 'surface') {
      console.log('Single', type, 'selected:', f.get('id'));
      if (f.get('id')) showRemovingOption(f.get('id'), true);
      // else { console.log('just drawn, no need for remove button'); }
      if (t[0] === 'MultiPoint') movingPoint = enableModify();
    }
  });
  map.addInteraction(select);

  const backToStart = () => {
    moveTo('home');
    forgetChanges();
    // select.dispatchEvent('select');
    select.getFeatures().clear();
    clearDrawings();
  };

  sendBtn.onclick = () => {
    console.log('Send features with new attributes to server');
    const ff = select.getFeatures();
    saveFeatures(ff);
    if (movingPoint?.getActive()) movePoint(ff.item(0));
    backToStart();
  };

  regretBtn.onclick = () => {
    resetToOriginalAttributes(select.getFeatures());
    if (movingPoint?.getActive()) requestGeometry(); // undo moved points
    backToStart();
  };

  // map.getTargetElement().onmouseenter = () => map.getTargetElement().focus();

  removeBtn.onclick = (e) => {
    removeById(e.target.dataset.id);
    backToStart();
  };
};

