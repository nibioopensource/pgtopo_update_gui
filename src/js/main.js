import '../css/loader.css';
import '../css/main.css';

import { listenToEvents } from './events';
import { drawInitialisation } from './map/draw';
// import { createPopup } from './map/popup';
import { getMap } from './map/tools';
import { initialize as createNotificationBar } from './notify';
import { listEditableLayers } from './request';
import * as wizard from './wizard';

createNotificationBar();

drawInitialisation();

wizard.initialise();

// createPopup();

listEditableLayers();

listenToEvents();

