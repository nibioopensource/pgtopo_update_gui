import Feature from 'ol/Feature';
import Point from 'ol/geom/Point';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';
import Circle from 'ol/style/Circle';
import Fill from 'ol/style/Fill';
import Stroke from 'ol/style/Stroke';
import Style from 'ol/style/Style';
// import Text from 'ol/style/Text';
import { Draw, Modify, Snap } from 'ol/interaction';

// import { sendFeature } from '../request'
import { getMap, getSelect } from './tools'
import * as wizard from '../wizard';

const map = getMap();

const s = new VectorSource();
const drawings = new VectorLayer({
  id: 'drawings',
  source: s,
  // style: new Style({
  //   fill: new Fill({
  //     color: 'rgba(255, 0, 0, 0.2)'
  //   }),
  //   stroke: new Stroke({
  //     color: '#ff0000',
  //     width: 2
  //   }),
  //   image: new Circle({
  //     radius: 7,
  //     fill: new Fill({
  //       color: '#ff0000'
  //     })
  //   })
  // })
});

let draw;
const modify = new Modify({ source: s });
const snap = new Snap({ source: s })

const addInteractions = () => {
  map.addInteraction(draw);
  // modify together with snap breaks double click on initial point:
  // map.addInteraction(modify);
  map.addInteraction(snap);
};
const removeInteractions = () => {
  map.removeInteraction(draw);
  // map.removeInteraction(modify);
  map.addInteraction(modify);
  map.removeInteraction(snap);
};

export const drawInitialisation = () => {
  map.addLayer(drawings);

  // draw.on('drawend', (e) => {
  s.on('addfeature', (e) => {
    const f = e.feature;
    if (f.getId() === 'p1') return;
    // console.log('drawend', drawings.getSource().getFeatures().length);

    // Keep id null to be able to detect when feature exists from before:
    // f.setId(f.ol_uid); // VectorSource#getFeatureById
    // f.setProperties({ id: f.ol_uid });

    // console.log(f.getGeometry().getCoordinates().length);
    // wizard.moveTo('confirmDrawing');
    wizard.moveTo('attributesSelection');
    // sendFeature(f);

    wizard.show();
    removeInteractions();
    isDrawing = false;
    // s.clear(); // remove all drawings on addfeature
    s.removeFeature(s.getFeatures()[0]); // remove only initial point on drawend
    getSelect().getFeatures().push(f);
    getSelect().dispatchEvent({ // to get attributes loaded on selectors
      type: 'select',
      selected: [f],
      deselected: []
    });
  });

};

const radioToDraw = { point: 'Point', path: 'LineString', surface: 'LineString' };

const newDrawType = () => {
  const radio = document.querySelector('input[name="geoRadio"]:checked').value;
  const type = radioToDraw[radio];

  draw = new Draw({ source: s, type, stopClick: true });

  // Enable snapping to initial point of current drawing:
  draw.on('drawstart', evt => {
    // console.log('drawstart');
    // cancelledDrawing = false;
    const sketch = evt.feature;
    const f1 = new Feature({
      geometry: new Point(sketch.getGeometry().getCoordinates()[0])
    });
    f1.setId('p1');
    f1.setStyle(new Style({
      image: new Circle({
        fill: new Fill({ color: 'rgba(255, 255, 255, 0.4)' }),
        stroke: new Stroke({ color: '#339900', width: 2 }),
        radius: 10
      })
    }));
    s.addFeature(f1);
  });
  return type;
};

let isDrawing = false;

drawBtn.onclick = () => {
  wizard.hide();
  if (isDrawing) {
    // console.log('remove interactions');
    removeInteractions();
    isDrawing = false;
  } else {
    // console.log('add interactions');
    isDrawing = newDrawType();
    addInteractions();
  }
};

export const clearDrawings = () => s.clear();

export const enableModify = () => {
  // s.addFeature(f);
  // console.log(s.getFeatures());
  const m = new Modify({ features: getSelect().getFeatures() });
  map.addInteraction(m);
  return m;
};
