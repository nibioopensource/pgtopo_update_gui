import { Circle, Fill, Stroke, Style, Text } from 'ol/style';

const fill = new Fill({ color: [255, 255, 255, .2] });
const stroke = new Stroke({ color: '#3399CC', width: 1.25 });
const defaultStyle = new Style({
  image: new Circle({ fill, stroke, radius: 5 }), fill, stroke
});
const hFill = new Fill({ color: [255, 0, 0, .1] });
const hStroke = new Stroke({ color: 'red', width: 2 });
const highlightStyle = new Style({
  image: new Circle({ fill: hFill, stroke: hStroke, radius: 5 }),
  fill: hFill,
  stroke: hStroke
});

const pLabel = document.getElementById('pLabelChoice');
const lLabel = document.getElementById('lLabelChoice');
const sLabel = document.getElementById('sLabelChoice');

export const idStyle = (highlighted) => {
  return (feature) => {
    const id = feature.get('id')?.toString();
    const pointId = new Style({ text: new Text({
      fill: new Fill({ color: highlighted ? 'red' : 'orange' }),
      offsetX: 14,
      offsetY: 8,
      scale: 2,
      stroke: new Stroke({ color: 'white' }),
      text: id
    }) });
    const lineId = new Style({ text: new Text({
      fill: new Fill({ color: highlighted ? 'red' : 'blue' }),
      scale: 2,
      stroke: new Stroke({ color: 'white' }),
      text: id
    }) });
    const surfaceId = new Style({ text: new Text({
      fill: new Fill({ color: highlighted ? 'red' : 'green' }),
      scale: 3,
      stroke: new Stroke({ color: 'white' }),
      text: id
    }) });
    const style = [highlighted ? highlightStyle : defaultStyle];
    const type = feature.getGeometry()?.getType();
    if (sLabel.checked && type.indexOf('Polygon') > -1) style.push(surfaceId);
    if (lLabel.checked && type.indexOf('LineString') > -1) style.push(lineId);
    if (pLabel.checked && type.indexOf('Point') > -1) style.push(pointId);
    return style;
  };
};
