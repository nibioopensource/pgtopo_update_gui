import { topologyLayer } from './tools';

export const removeFeatureById = id => {
  const s = topologyLayer.getSource();
  // s.removeFeature(s.getFeatureById(id));
  const n = Number(id);
  s.forEachFeature(f => { if (f.get('id') === n) return s.removeFeature(f); });
};
