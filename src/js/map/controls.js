import { Control } from 'ol/control';

export class WizardSwitch extends Control {
  constructor(opt_options) {
    const options = opt_options || {};

    const button = document.createElement('button');
    button.innerHTML = 'Edit';

    const element = document.createElement('div');
    element.id = 'wizardSwitch';
    element.className = 'switch ol-unselectable ol-control';
    element.title = 'Show/Hide';
    element.appendChild(button);

    super({ element: element, target: options.target });

    button.addEventListener('click', this.toggleWizard.bind(this), false);
  };

  toggleWizard = () => {
    const w = document.getElementById('wizard');
    const isHidden = (w.style.display === 'none');
    w.style.display = isHidden ? 'block' : 'none';
  };
}

