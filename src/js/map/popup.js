import '../../css/popup.css';

import { getMap } from './tools';
import { sendFeaturesToBackend } from '../request';

import Overlay from 'ol/Overlay';

const map = getMap();

export const createPopup = () => {
  const element = document.getElementById('popup');
  const x = document.getElementById('popupCloser');

  const popup = new Overlay({ element: element, autoPan: true });
  x.onclick = () => {
    popup.setPosition(undefined);
    x.blur();
    return false;
  };
  map.addOverlay(popup);

  map.on('click', e => {
    // if (drawingInteraction is active) return; // if not drawing
    const features = map.getFeaturesAtPixel(
      e.pixel, { layerFilter: l => l.get('id') === 'topology' }
    );
    const numberOfFeatures = features.length;
    if (numberOfFeatures) {
      const popupContent = document.getElementById('popupContent');
      popupContent.innerHTML = '';
      const [f, pos] = nextFeature(features);
      if (numberOfFeatures > 1)
        popupContent.innerHTML = `${pos + 1} of ${numberOfFeatures} features`;
      popupContent.appendChild(displayedFeature(f));
      popup.setPosition(e.coordinate);
    } else {
      popup.setPosition(undefined); // Hide popup
      // removeFeatureHighlight();
    }
  });
};

const nextFeature = (features) => {
  const numberOfFeatures = features.length;
  let relativePosition = -1;
  for (let i = 0; i < numberOfFeatures; i++) {
    const f = features[i];
    const id = f.get('id');
    if (f.get('selected')) {
      relativePosition = i;
      // f.set('selected', false);
      break;
    }
  }
  const nextPosition = (relativePosition + 1) % numberOfFeatures;
  const nextFeature = features[nextPosition];
  // nextFeature.set('selected', true);
  return [nextFeature, nextPosition];
};

const displayedFeature = (f) => {
  const skip = ['geometry', 'selected'];
  const ul = document.createElement('ul');
  const p = f.getProperties();
  Object.entries(p).forEach(([key, value]) => {
    // console.log(key, value);
    if (skip.indexOf(key) < 0) {
      const li = document.createElement('li');
      li.innerHTML = `${key}: ${value}`;
      ul.appendChild(li);
    }
  });
  return ul;
};

