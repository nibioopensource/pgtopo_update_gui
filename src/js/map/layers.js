// // Modules
// import { getTopLeft } from 'ol/extent';
// import ImageLayer from 'ol/layer/Image';
// import ImageWMS from 'ol/source/ImageWMS';
// import TilegridWMTS from 'ol/tilegrid/WMTS';
// import WMTS from 'ol/source/WMTS';
// import TileLayer from 'ol/layer/Tile';
// import TileWMS from 'ol/source/TileWMS';
// import VectorLayer from 'ol/layer/Vector';
// import VectorSource from 'ol/source/Vector';
// import Circle from 'ol/style/Circle';
// import Fill from 'ol/style/Fill';
// import Stroke from 'ol/style/Stroke';
// import Style from 'ol/style/Style';
//
// import { get as getProjection } from 'ol/proj';

let drawnSource = new VectorSource({ wrapX: false });
const drawnVectorLayer = new VectorLayer({
  id: 'drawnVectorLayer',
  source: drawnSource,
  style: new Style({
    fill: new Fill({
      color: 'rgba(255, 255, 255, 0.5)'
    }),
    stroke: new Stroke({
      color: '#2196F3',
      width: 3
    }),
    image: new Circle({
      radius: 7,
      fill: new Fill({
        color: '#ffcc33'
      })
    })
  })
});

