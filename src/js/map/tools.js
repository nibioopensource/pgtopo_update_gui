import {
  Attribution, defaults as defaultControls, MousePosition
} from 'ol/control';
import { platformModifierKeyOnly } from 'ol/events/condition';
import { boundingExtent } from 'ol/extent';
import TopoJSON from 'ol/format/TopoJSON';
import {
  defaults as defaultInteractions, DragBox, Select
} from 'ol/interaction';
import TileLayer from 'ol/layer/Tile';
import VectorLayer from 'ol/layer/Vector';
import Map from 'ol/Map';
import OSM from 'ol/source/OSM';
import VectorSource from 'ol/source/Vector';
import View from 'ol/View';
// import ZoomSlider from 'ol/control/ZoomSlider';

import proj4 from 'proj4';
import { get as getProjection, transform, transformExtent } from 'ol/proj';
import { register } from 'ol/proj/proj4';

import { WizardSwitch } from './controls';
import { idStyle } from '../map/styles';
import { requestGeometry } from '../request';
import { changingTo, rememberAsOriginalAttributes } from '../storage';

// Setup projections for map
// EUREF89 (Norway)
proj4.defs('EPSG:25832', '+proj=utm +zone=32 +ellps=GRS80 +units=m +no_defs');
proj4.defs('EPSG:25833', '+proj=utm +zone=33 +ellps=GRS80 +units=m +no_defs');
proj4.defs('EPSG:25835', '+proj=utm +zone=35 +ellps=GRS80 +units=m +no_defs');
proj4.defs('EPSG:4258', '+proj=longlat +ellps=GRS80 +no_defs');

proj4.defs(
  'EPSG:3003',
  `+proj=tmerc +lat_0=0 +lon_0=9 +k=0.9996 +x_0=1500000 +y_0=0 +ellps=intl
  +towgs84=-104.1,-49.1,-9.9,0.971,-2.917,0.714,-11.6799999999584
  +units=m +no_defs +type=crs`
);
proj4.defs(
  'EPSG:3035',
  `+proj=laea +lat_0=52 +lon_0=10 +x_0=4321000 +y_0=3210000
  +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs +type=crs`
);

register(proj4);

const epsg = 'EPSG:3035';
let previousExtent = [0, 0, 1, 1];

export const topologyLayer = new VectorLayer({
  id: 'topology',
  source: new VectorSource({}),
  style: idStyle(false)
});
pLabelChoice.onchange = () => topologyLayer.changed();
lLabelChoice.onchange = () => topologyLayer.changed();
sLabelChoice.onchange = () => topologyLayer.changed();

const map = new Map({
  target: 'map',
  layers: [ new TileLayer({ source: new OSM() }), topologyLayer ],
  view: new View({
    center: transform([402121, 7287106], 'EPSG:25833', epsg),
    projection: getProjection(epsg),
    zoom: 6
  }),
  controls: defaultControls({ attribution: false }).extend([
    new Attribution({ collapsible: true }),
    new MousePosition({
      coordinateFormat: c => {
        const epsg = map.getView().getProjection().getCode();
        return `${epsg}: [${Math.round(c[0])}, ${Math.round(c[1])}]`
      },
    }),
    // new ZoomSlider,
    new WizardSwitch
  ]),
  // interactions: defaultInteractions().extend([select])
});

const dragBox = new DragBox({ condition: platformModifierKeyOnly });
map.addInteraction(dragBox);
dragBox.on('boxend', () => {
  const e = dragBox.getGeometry().getExtent();
  const s = topologyLayer.getSource();
  const sf = select.getFeatures();
  const boxFeatures = s.getFeaturesInExtent(e).filter(
    f => f.getGeometry().intersectsExtent(e) && sf.getArray().indexOf(f) < 0
  );

  // features that intersect the box geometry are added to the
  // collection of selected features

  // if the view is not obliquely rotated the box geometry and
  // its extent are equalivalent so intersecting features can
  // be added directly to the collection
  const rotation = map.getView().getRotation();
  const oblique = rotation % (Math.PI / 2) !== 0;

  // when the view is obliquely rotated the box extent will
  // exceed its geometry so both the box and the candidate
  // feature geometries are rotated around a common anchor
  // to confirm that, with the box geometry aligned with its
  // extent, the geometries intersect
  if (oblique) {
    const anchor = [0, 0];
    const geometry = dragBox.getGeometry().clone();
    geometry.rotate(-rotation, anchor);
    const extent = geometry.getExtent();
    boxFeatures.forEach(feature => {
      const geometry = feature.getGeometry().clone();
      geometry.rotate(-rotation, anchor);
      if (geometry.intersectsExtent(extent)) sf.push(feature);
    });
  } else sf.extend(boxFeatures);
});

const coordinateOnBoundary = (c, f) => {
  const cp = f.getGeometry().getClosestPoint(c);
  return cp[0] === c[0] && cp[1] === c[1];
};

const customGetFeaturesAtCoordinate = (c, s) => {
  const e = boundingExtent([c.map(xy => xy - .001), c.map(xy => xy + .001)]);
  return s.getFeaturesInExtent(e).filter(f =>
    f.getGeometry().intersectsExtent(e) && coordinateOnBoundary(c, f)
  );
};

const dimensionFromType = (type) => {
  if (type.indexOf('Point') > -1) return 0;
  else if (type.indexOf('LineString') > -1) return 1;
  else if (type.indexOf('Polygon') > -1) return 2;
  else return null;
};

const segmentIsOutsidePolygon = ([a, b], p) => {
  const c = [(b[0] + a[0]) / 2, (b[1] + a[1]) / 2];
  const e = boundingExtent([c.map(xy => xy - .001), c.map(xy => xy + .001)]);
  return !p.intersectsExtent(e);
};

const oneIsBorderOfTheOther = (f1, f2) => {
  const f = [f1, f2];
  const t = f.map(fi => fi.getGeometry().getType());
  const d = t.map(ti => dimensionFromType(ti));
  const higher = d[0] > d[1] ? f[0] : f[1];
  const lower = d[0] > d[1] ? f[1] : f[0];
  const checkingCoordinates = featureFlatCoordinates(lower);
  const l = checkingCoordinates.length;
  for (let i = 0; i < l; i++) {
    const c = checkingCoordinates[i];
    if (!coordinateOnBoundary(c, higher)) return false;
  }
  // One case is left: when linestring is made of two points,
  // both part of polygon boundary too, but the linestring segment is outside.
  if (l === 2 && segmentIsOutsidePolygon(
    checkingCoordinates, higher.getGeometry()
  )) return false;
  return true;
};

const featureFlatCoordinates = (f) => {
  // const multi = geo.getType().indexOf('Multi') > -1;
  const coordinates = f.getGeometry().getCoordinates();
  return coordinates.flat(Infinity).reduce((result, value, i, array) => {
    if (i % 2 === 0) result.push(array.slice(i, i + 2));
    return result;
  }, []);
};

const indexById = (ff, id) => ff.some(f => f.get('id') === id);

export const getAdjacentFeatures = (feature, type) => {
  // const id = feature.get('id'); // can be same for a polygon and a linestring
  const id = feature.ol_uid;
  const s = topologyLayer.getSource();
  // Something wrong: duplicate polygons and missing linestring (with same id)
  // s.getFeatures().forEach(f => console.log(f.getProperties()));
  const adjacent = [];
  featureFlatCoordinates(feature).forEach((c, i) => {
    // console.log(`${i + 1}. coordinate`, adjacent.map(a => a.get('id')));
    // const all = s.getFeaturesAtCoordinate(c); // faulty when c is on boudary
    const all = customGetFeaturesAtCoordinate(c, s);
    // console.log('Not filtered', all.map(f => f.getProperties()));
    const filtered = all.filter(
      f => f.ol_uid !== id && f.getGeometry().getType() === type
    );
    // ).filter((value, index, array) => array.indexOf(value) === index); // unique
    // console.log('Filtered', filtered.map(f => f.getProperties()));
    // console.log('Filtered', filtered.map(f => f.get('id')));
    // if (i === 0) adjacent.push(...filtered);
    // else {
      // const intersection = adjacent.filter(f => filtered.includes(f));
      // const union = [...new Set(adjacent.concat(filtered))];
      filtered.forEach(f => {
        if (
          !adjacent.includes(f)
          && !indexById(adjacent, f.get('id'))// looks like bug on s.getFeatures
        ) adjacent.push(f);
      });
    // }
  });
  return adjacent.filter(f => oneIsBorderOfTheOther(f, feature));
};

export const getBoundingBoxDiagonal = (srid) => {
  const src = map.getView().getProjection().getCode();
  const dst = srid ? `EPSG:${srid}` : src;
  const extent = map.getView().calculateExtent(map.getSize());
  const e = transformExtent(extent, src, dst);
  // console.log('diagonal', [e.slice(0, 2), e.slice(-2)]);
  return {
    type: 'LineString',
    crs: { type: 'name', properties: { name: dst } },
    coordinates: [e.slice(0, 2), e.slice(-2)]
  }
};

export const getEPSG = () => map.getView().getProjection().getCode();

export const getMap = () => map;

export const getLayerById = (id) => {
  const layers = map.getLayers();
  const nl = layers.getLength();
  for (let i = 0; i < nl; i++) {
    const l = layers.item(i);
    if (l.get('id') === id) return l;
  }
};

export const getLayerIdByFeature = (f) => {
  const layers = map.getLayers();
  const nl = layers.getLength();
  for (let i = 0; i < nl; i++) {
    const l = layers.item(i);
    if (l.getSource().hasFeature(f)) return l.get('id');
  }
  return null;
};

export const getLegendUrl = (wmsName, layerName) => {
  const nwu = getLayerById(wmsName).getSource().getLegendUrl(undefined, layerName);
  const v1 = nwu.replace('1.3.0', '1.1.1');
  const wu = v1.split('LAYER=')[0]
  // return v1.split('%2C')[0];
  return wu + 'LAYER=' + layerName;
};

export const removeGeometry = () => topologyLayer.getSource().clear();

export const loadGeometry = (topojson, firstTime) => {
  console.log('Load topojson geometry to openlayers');
  // console.log(topojson, 'check projection used here, line 110');
  // if (topojson.objects.arcs.arcs) topojson.arcs = topojson.objects.arcs.arcs;
  const features = new TopoJSON({
    // dataProjection: getProjection('EPSG:4326')
    // dataProjection: getProjection('EPSG:3003')
    // dataProjection: getProjection('EPSG:4258')
  }).readFeatures(topojson);
  // }).readFeatures(JSON.stringify(topojson));
  topologyLayer.getSource().addFeatures(features);
  // topologyLayer.getSource().changed();
  if (firstTime) {
    // map.getView().fit(
    //   topologyLayer.getSource().getExtent(),
    //   { duration: 2000, padding: [50, 50, 50, 50] }
    // );
    previousExtent = map.getView().calculateExtent(map.getSize());
  }
  rememberAsOriginalAttributes(features);
};

const almostSameExtent = () => {
  // console.log('almost same extent?');
  const newExtent = map.getView().calculateExtent(map.getSize());
  // const sw = getBottomLeft(newExtent);
  const sw = newExtent.slice(0, 2);
  // const ne = getTopRight(newExtent);
  const ne = newExtent.slice(-2);
  const d = Math.sqrt(Math.pow(ne[0] - sw[0], 2) + Math.pow(ne[1] - sw[1], 2));
  const sw_ = previousExtent.slice(0, 2);
  const ne_ = previousExtent.slice(-2);
  const d_ = Math.sqrt(Math.pow(ne_[0] - sw_[0], 2) + Math.pow(ne_[1] - sw_[1], 2));
  previousExtent = newExtent;
  const dChange = Math.abs(d - d_);
  const dPercentage = dChange * 100 / d_;
  // console.log('diagonal', d_, d, dChange, dPercentage);
  const a = sw[0] - sw_[0];
  const b = sw[1] - sw_[1];
  const displacement = Math.sqrt( a*a + b*b );
  const movePercentage = displacement * 100 / d_;
  // console.log('displacement', a, b, displacement, movePercentage);
  if ((dPercentage < 5) && (movePercentage < 5)) return true; // (same extent)
  return false;
};

export const updateGeometry = () => {
  if (almostSameExtent()) return; else requestGeometry();
};

const select = new Select({
  style: idStyle(true),
  toggleCondition: platformModifierKeyOnly
});
export const getSelect = () => select;

export const updateSelected = (e) => {
  const key = e.name;
  const v = e.value;
  // v is always string, convert to number what came as number from backend
  const selector = (e.tagName.toLowerCase() === 'select');
  const isNumber = ((+v).toString() === v);
  const numericalOption = (selector && isNumber);
  const value = numericalOption ? +v : v;
  // console.log(key, value);
  select.getFeatures().forEach(f => f.set(key, value));
  changingTo(key, value);
};

