import { initializeAttributesSelection } from './wizard';

const checkOrHide = (g, configuration) => {
  const present = configuration[`${g}_layer`] ? true : false;
  const radio = document.querySelector(`input[name="geoRadio"][value="${g}"]`);
  const label = document.querySelector(`label[for="${g}Radio"]`);
  radio.checked = present;
  radio.hidden = !present;
  label.hidden = !present;
  return present ? 0 : 1;
};

export const readDatasetConfiguration = c => {
  const attributes = c.tables[0].attributes;
  initializeAttributesSelection(attributes);
  let remaining = 3;
  ['point', 'path', 'surface'].forEach(g => remaining -= checkOrHide(g, c));
  const chosenRadio = document.querySelector('input[name="geoRadio"]:checked');
  const single = remaining === 1;
  chosenRadio.hidden = single;
  document.querySelector(`label[for="${chosenRadio.id}"]`).hidden = single;
};
