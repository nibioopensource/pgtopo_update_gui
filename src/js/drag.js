// Make the DIV element draggagle:
// https://www.w3schools.com/howto/howto_js_draggable.asp

export const dragElement = (elmntId, handleId) => {
  const elmnt = document.getElementById(elmntId);
  const handle = document.getElementById(handleId);
  let pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  if (handle) {
    // if present, the handle is where you move the DIV from:
    handle.onmousedown = dragMouseDown;
    handle.ontouchstart = dragTouchStart;
  } else {
    // otherwise, move the DIV from anywhere inside the DIV:
    elmnt.onmousedown = dragMouseDown;
    elmnt.ontouchstart = dragTouchStart;
  }
  function dragMouseDown(e) { // MouseEvent
    e.preventDefault(); // avoid IPad scrolling bounce effect
    elmnt.style.opacity = '0.7';
    window.addEventListener('selectstart', disableSelection, false);
    // e = e || window.event;
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }
  function dragTouchStart(e) { // TouchEvent
    e.preventDefault(); // avoid IPad scrolling bounce effect
    elmnt.style.opacity = '0.7';
    window.addEventListener('selectstart', disableSelection, false);
    // e = e || window.event;
    // get the mouse cursor position at startup:
    pos3 = e.touches[0].clientX;
    pos4 = e.touches[0].clientY;
    document.ontouchend = closeDragElement;
    // call a function whenever the cursor moves:
    document.ontouchmove = elementDrag;
  }
  function elementDrag(e) { // MouseEvent | TouchEvent
    // const generalized = e ? e : window.event;
    const x = (e instanceof MouseEvent) ? e.clientX : e.touches[0].clientX;
    const y = (e instanceof MouseEvent) ? e.clientY : e.touches[0].clientY;
    // calculate the new cursor position:
    pos1 = pos3 - x;
    pos2 = pos4 - y;
    pos3 = x;
    pos4 = y;
    // set the element's new position:
    const newTop = (elmnt.offsetTop - pos2);
    // elmnt.style.top = Math.max(4, newTop) + 'px';
    elmnt.style.top = `min(calc(100% - 40px), ${Math.max(4, newTop)}px)`;
    const newLeft = (elmnt.offsetLeft - pos1);
    // elmnt.style.left = Math.max(4, newLeft) + 'px';
    elmnt.style.left = `min(calc(100% - 40px), ${Math.max(4, newLeft)}px)`;
  }
  function closeDragElement() {
    elmnt.style.opacity = '1';
    window.removeEventListener('selectstart', disableSelection, false);
    /* stop moving when mouse button is released:*/
    document.onmouseup = null;
    document.ontouchend = null;
    document.onmousemove = null;
    document.ontouchmove = null;
  }
  function disableSelection(event) { // Event
    event.preventDefault();
  }
}

