export default {
  build: {
    sourcemap: true,
    emptyOutDir: true,
    outDir: '../dist'
  },
  root: 'src'
}
